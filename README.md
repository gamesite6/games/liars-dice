# Liar's Dice

## Example requests

### Initial state

```json
{
  "seed": 42,
  "settings": {
    "selected-player-counts": [3, 4, 5],
  },
  "players": [1, 2, 3]
}
```
