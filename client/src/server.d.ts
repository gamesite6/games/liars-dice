type PlayerId = number;

type GameSettings = {
  "selected-player-counts": number[];
};

type GameState = {
  phase: Phase;
  players: Player[];
};

type Die = 1 | 2 | 3 | 4 | 5 | 6;

type Player = {
  id: PlayerId;
  dice: Die[];
};

type Phase = LeadingPhase | FollowingPhase;

type LeadingPhase = { kind: "leading"; player: PlayerId };
type FollowingPhase = {
  kind: "following";
  player: PlayerId;
  die: Die;
  count: number;
  leader: PlayerId;
};

type Action =
  | { kind: "bid"; die: Die; count: number }
  | { kind: "challenge" }
  | { kind: "ready" };
