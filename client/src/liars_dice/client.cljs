(ns liars-dice.client
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]
            [liars-dice.shared :as shared]))

(defn shown-die [die]
  [:gs6-die {:class "block m-[0.2em]"
             :value (or die 0)}])

(defn inline-die [die]
  [:gs6-die {:class "inline-block mx-[0.25em] translate-y-[0.25em]"
             :value (or die 0)}])

(defn player-box [player blank-dice thinking buttons]
  [:gs6-player-box {:userid (:id player) :thinking (if thinking true nil)}
   [:div
    {:class "absolute -top-8 left-1/2 -translate-x-1/2 -translate-y-full"}
    buttons]
   [:ul
    {:class "text-xl gap-1 flex justify-center absolute left-1/2 bottom-1 -translate-x-1/2 translate-y-full"}
    (if blank-dice
      (for [_ (:dice player)]
        [:li [shown-die]])
      (for [die (:dice player)]
        [:li [shown-die die]]))]])

(defn with-user-first
  "Rotates players until the user is first."
  [user-id players]
  (->> players
       cycle
       (take (* 2 (count players)))
       (drop-while  #(and (not (nil? user-id)) (not= (:id %) user-id)))
       (take (count players))))

(defn player-order
  [user-id players]
  (let [player-ids (vec (map :id players))
        idx (.indexOf player-ids user-id)]
    (if (not= idx -1)

      (concat (subvec player-ids idx)
              (subvec player-ids 0 idx))

      player-ids)))


(defn dice-count [player] (count (:dice player)))

(defn all-dice [{players :players}]
  (apply concat (map :dice players)))

(defn total-dice-count [state]
  (apply + (map dice-count (:players state))))

(defn parse-int [s]
  (let [parsed (js/parseInt s)]
    (if (int? parsed)
      parsed
      nil)))

(defmulti action-buttons (fn [state perform-action] (:kind (:phase state))))

(defmethod action-buttons "game-complete" [_ _] nil)

(defmethod action-buttons "challenge-result"
  [state perform-action]
  [:div
   {:class "flex flex-col items-center"}
   [:button.primary
    {:on-click #(perform-action {:kind "ready"})}
    "Ready"]])

(defmethod action-buttons "final-challenge-result"
  [state perform-action]
  [:div
   {:class "flex flex-col items-center"}
   [:button.primary
    {:on-click #(perform-action {:kind "ready"})}
    "Ready"]])

(defmethod action-buttons "leading"
  [state perform-action]
  (let [min-count 1
        max-count (total-dice-count state)
        counts (range max-count (- min-count 1) -1)
        selected-die (r/atom nil)
        selected-count (r/atom min-count)]
    [(fn []
       [:form
        {:class "flex flex-col items-center gap-4"
         :on-submit #(do (.preventDefault %)
                         (perform-action {:kind "bid" :die @selected-die :count @selected-count}))}
        [:div
         {:class "flex items-center gap-4"}
         [:select {:on-change (fn [evt] (reset! selected-count (-> evt .-target .-value parse-int)))}
          (for [c counts]
            [:option {:value c :selected (= @selected-count c)} c])]
         " × "
         [:ul
          {:class "grid grid-cols-[repeat(3,1fr)] gap-x-2 gap-y-1"}
          (for [die (range 1 7)]
            [:li {:key die}
             [:label {:class "whitespace-nowrap"}
              [:input {:type "radio"
                       :id (str "bid-die-" die)
                       :name "bid-die"
                       :required true
                       :on-click #(reset! selected-die die)}]
              [inline-die die]]])]]
        [:button.primary {:type "submit"
                          :disabled (or (nil? @selected-die) (nil? @selected-count))} "Place bid"]])]))

(defmethod action-buttons "final-leading" [state perform-action]
  (let [min-bid      2
        max-bid      12
        bids         (range max-bid (- min-bid 1) -1)
        selected-bid (r/atom min-bid)]
    [(fn []
       [:form {:class     "flex flex-col items-center gap-4 w-40"
               :on-submit #(do (.preventDefault %)
                               (perform-action {:kind "final-bid"
                                                :bid  @selected-bid}))}
        [:select {:on-change (fn [evt] (reset! selected-bid (-> evt .-target .-value parse-int)))}
         (for [bid bids]
           [:option {:value    bid
                     :selected (= @selected-bid bid)} bid])]
        [:button.primary {:type     "submit"
                          :disabled (nil? @selected-bid)} "Place bid"]])]))


(defmethod action-buttons "final-following" [state perform-action]
  (let [is-raising (r/atom false)
        min-bid      (+ 1 (-> state :phase :bid))
        max-bid      12]
    [(fn []
       (if @is-raising

         (let [bids         (range max-bid (- min-bid 1) -1)
               selected-bid (r/atom min-bid)]
           [(fn []
              [:form {:class     "flex flex-col items-center gap-4 w-60"
                      :on-submit #(do (.preventDefault %)
                                      (perform-action {:kind "final-bid"
                                                       :bid  @selected-bid}))}
               [:select {:on-change (fn [evt] (reset! selected-bid (-> evt .-target .-value parse-int)))}
                (for [bid bids]
                  [:option {:value    bid
                            :selected (= @selected-bid bid)} bid])]
               [:div {:class "flex justify-center gap-1"}
                [:button.secondary {:type "button" :on-click #(reset! is-raising false)} "Cancel"]
                [:button.primary {:type     "submit"
                                  :disabled (nil? @selected-bid)} "Place bid"]]])])
         [:div
          {:class "flex justify-center gap-1"}
          [:button.secondary {:disabled (> min-bid max-bid) :on-click #(reset! is-raising true)} "Raise"]
          [:button.primary {:on-click #(perform-action {:kind :challenge})} "Challenge!"]]))]))

(defmethod action-buttons "following"
  [{{previous-count :count previous-die :die} :phase :as state} perform-action]
  (let [is-raising (r/atom false)
        min-count previous-count
        max-count (total-dice-count state)]
    [(fn []
       (if @is-raising
         (let [counts (range max-count (- min-count 1) -1)
               selected-die (r/atom nil)
               selected-count (r/atom min-count)]
           [(fn []
              (let [is-valid (and (not (nil? @selected-die))
                                  (not (nil? @selected-count))
                                  (<= previous-count @selected-count)
                                  (or (< previous-count @selected-count) (< previous-die @selected-die)))]
                [:form
                 {:class "flex flex-col items-center gap-4"
                  :on-submit #(do (.preventDefault %)
                                  (perform-action {:kind "bid" :die @selected-die :count @selected-count}))}
                 [:div
                  {:class "flex items-center gap-4"}
                  [:select {:on-change (fn [evt] (reset! selected-count (-> evt .-target .-value parse-int)))}
                   (for [c counts]
                     [:option {:value c :selected (= @selected-count c)} c])]
                  " × "
                  [:ul
                   {:class "grid grid-cols-[repeat(3,1fr)] gap-x-2 gap-y-1"}
                   (for [die (range 1 7)]
                     [:li [:label {:class "whitespace-nowrap"}
                           [:input {:type "radio"
                                    :id (str "bid-die-" die)
                                    :name "bid-die"
                                    :required true
                                    :on-click #(reset! selected-die die)}]
                           [inline-die die]]])]]

                 [:div
                  {:class "flex justify-center gap-1"}
                  [:button.secondary {:type "button" :on-click #(reset! is-raising false)} "Cancel"]
                  [:button.primary {:type "submit" :disabled (not is-valid)} "Place bid"]]]))])
         [:div
          {:class "flex justify-center gap-1"}
          [:button.secondary {:disabled (and (= min-count max-count) (= previous-die 6))
                              :on-click #(reset! is-raising true)} "Raise"]
          [:button.primary {:on-click #(perform-action {:kind :challenge})} "Challenge!"]]))]))


(defn player-list [user-id {players :players :as state} perform-action]
  [:ul
   (let [ordered (player-order user-id players)]

     (map-indexed
      (fn [_ {player-id :id :as player}]
        (let [idx (.indexOf ordered player-id)]
          [:li
           {:key player-id
            :class "pb-8 absolute top-1/2 left-1/2 transition-transform duration-500"
            :style (let [startAngle (/ Math/PI 2)
                         angle (+ startAngle (* (/ (* Math/PI 2) (count players)) idx))
                         x (* 22 (Math/cos angle))
                         y (* 20 (Math/sin angle))]
                     {:transform (str "translate(-50%,-50%) "
                                      "translateX(" x "rem) "
                                      "translateY(" y "rem)")})}
           (let [show-dice (or (= player-id user-id)
                               (boolean (#{"challenge-result" "final-challenge-result" "game-complete"} (-> state :phase :kind))))]
             [player-box player (not show-dice) (shared/acting? state player-id)
              (if (and (= player-id user-id) (shared/acting? state user-id))
                [action-buttons state perform-action]
                nil)])]))
      players))])



(defmulti phase-description (fn [state] (:kind (:phase state))))

(defmethod phase-description "leading"
  [{{player-id :player} :phase}]
  [:div
   [:gs6-user {:userid player-id}]
   " must place a starting bid."])

(defmethod phase-description "following"
  [{{player-id :player leader-id  :leader die-count :count die-value :die} :phase}]
  [:div
   {:class "flex flex-col gap-4"}
   [:div [:gs6-user {:userid leader-id}] " bid"]
   [:div
    {:class "flex flex-nowrap justify-center items-center gap-2"}
    [:span {:class "text-4xl"}  die-count " × "]
    [:span {:class "text-5xl"} [shown-die die-value]]]

   [:div [:gs6-user {:userid player-id}] " must raise or challenge."]])

(defmethod phase-description "challenge-result"
  [{{challenger-id :challenger leader-id :leader die-count :count die-value :die} :phase :as state}]
  (let [successful-bid (shared/successful-bid? state)
        actual-die-count (count (filter #(= % die-value) (all-dice state)))
        loser-id (if successful-bid challenger-id leader-id)]
    [:div
     {:class "flex flex-col gap-4"}
     [:div [:gs6-user {:userid leader-id}] " bid"]
     [:div
      {:class "flex flex-nowrap justify-center items-center gap-2"}
      [:span {:class "text-4xl"} die-count " × "]
      [:span {:class "text-5xl"} [shown-die die-value]]]
     [:div "Challenged by " [:gs6-user {:userid challenger-id}] "!"]
     [:div
      "There are "
      (if (not successful-bid) " only " nil)
      actual-die-count
      " "
      [inline-die die-value]
      "'s."
      [:br]
      [:gs6-user {:userid loser-id}] " loses a die!"]]))

(defmethod phase-description "final-challenge-result"
  [{{challenger-id :challenger leader-id :leader bid :bid} :phase :as state}]
  [:div {:class "flex flex-col justify-center gap-1 relative"}
   [:div {:class "panel text-xs text-left absolute -top-4 -translate-y-full"} "There are only two dice left. Players now bid the sum of the two dice."]
   [:span [:gs6-user {:userid leader-id}] " bid"]
   [:span {:class "text-6xl font-medium"} bid]
   [:span [:gs6-user {:userid challenger-id}] " challenged."]
   (let [dice-total (->> state :players (map :dice) flatten (apply +))
         loser-id (if (> bid dice-total) leader-id challenger-id)]
     [:div {:class "absolute -bottom-4 translate-y-full"}
      "The sum was "
      [:strong dice-total]
      ". "
      [:br]
      [:gs6-user {:userid loser-id}] " loses their last die!"])])

(defmethod phase-description "final-leading"
  [{{player-id :player} :phase}]
  [:div {:class "relative"}
   [:div {:class "panel text-xs text-left absolute -top-4 -translate-y-full"} "There are only two dice left. Players now bid the sum of the two dice."]
   [:p [:gs6-user {:userid player-id}] " must place a starting bid."]])


(defmethod phase-description "final-following"
  [{{player-id :player leader-id :leader bid :bid} :phase}]
  [:div {:class "flex flex-col justify-center gap-1 relative"}
   [:div {:class "panel text-xs text-left absolute -top-4 -translate-y-full"} "There are only two dice left. Players now bid the sum of the two dice."]
   [:span [:gs6-user {:userid leader-id}] " bid"]
   [:span {:class "text-6xl font-medium"} bid]
   [:span [:gs6-user {:userid player-id}] " must raise or challenge."]])

(defmethod phase-description "game-complete" [{players :players}]
  (let [winner (some #(when (shared/has-dice %) %) players)]
    [:h2
     [:gs6-user {:userid (:id winner)}] " wins!"]))



(defn ^:export renderGame [el user-id settings state perform-action]
  (let [settings (js->clj settings :keywordize-keys true)
        state (js->clj state :keywordize-keys true)
        perform-action (fn [action] (perform-action (clj->js action)))]
    (rdom/render
     [:main
      [:div {:class "absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2"}
       [:div
        {:class "w-60 text-center"}
        [phase-description state]]]
      [player-list user-id state perform-action]]
     el)))

(defn ^:export renderReference [el settings]
  (rdom/render
   [:div.panel
    {:class "h-full bg-[linen] text-[85%] overflow-y-auto"}
    [:h3.font-medium "On your turn"]
    [:dl
     [:dt {:class "font-medium"} "Place a bid (quantity and dice face)"]
     [:dd {:class "m-0"}
      [:p.mt-0.mb-2 "The quantity represents how many of the chosen die face have been rolled in total by all players. "]
      [:p.my-2 "If a player has already placed a bid, your bid must either be the same quantity of a higher face value, or a higher quantity of any face value. "]
      [:p.mt-2.mb-0 "You may not lower the quantity. "]]

     [:div {:class "text-center font-medium m-3"} "~ or ~"]

     [:dt {:class "font-medium"} "Challenge the previous bid"]
     [:dd {:class "m-0"}
      [:p.mt-0.mb-2 "Every player reveals their dice. "]
      [:p.my-2 "If there are fewer dice showing the bid face value than the bid quantity, your challenge is successful, and the bidder loses one of their dice. "]
      [:p.my-2 "However, if there are a matching or higher number of dice, your challenge fails, and you lose a die instead. "]
      [:p.mt-2.mb-0 "Whoever lost a die begins the next round."]]]

    [:h3.font-medium "End of the game"]
    [:p.mt-0.mb-2 "The last player with dice remaining wins!"]
    [:p.mt-2.mb-0 "If only two players remain with a single die each, they bid the sum of the two dice."]]
   el))

(defn ^:export renderSettings [el settings on-change read-only]
  (let [settings (js->clj settings :keywordize-keys true)
        player-counts-set (set (:selected-player-counts settings))
        handle-player-count-change (fn [count] (fn [evt]
                                                 (let [checked      (-> evt .-target .-checked)
                                                       new-settings (if checked
                                                                      (assoc settings :selected-player-counts (conj player-counts-set count))
                                                                      (assoc settings :selected-player-counts (disj player-counts-set count)))]
                                                   (on-change (clj->js new-settings)))))]
    (rdom/render
     [:div {}
      [:fieldset
       {:class "border-none p-0 m-0"}
       [:legend "Player counts"]
       [:ul {:class "m-0 flex gap-4"}
        (for [count shared/valid-player-counts]
          [:li {:key count}
           [:label {}
            [:input {:type "checkbox"
                     :checked (boolean (player-counts-set count))
                     :disabled read-only
                     :on-click (handle-player-count-change count)}]
            count]])]
       [:div {:class "flex gap-4 opacity-90"}
        [:small "Recommended: 3-6"] [:small "Best: 5-6"]]]] el)))
