import { renderGame, renderReference, renderSettings } from "../lib/liars-dice";
import "./style.css";

export class Game extends HTMLElement {
  #state?: GameState;
  #settings?: GameSettings;
  #playerId?: PlayerId;

  set state(state: GameState) {
    if (this.#state !== state) {
      this.#state = state;
      this.render();
    }
  }

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.render();
    }
  }

  set playerid(playerId: PlayerId | undefined) {
    if (this.#playerId !== playerId) {
      this.#playerId = playerId;
      this.render();
    }
  }

  #handleAction: (action: Action) => void;

  constructor() {
    super();

    this.#handleAction = (action: Action) => {
      this.dispatchEvent(
        new CustomEvent("action", {
          detail: action,
          bubbles: false,
        })
      );
    };
  }

  private render() {
    if (this.#state && this.#settings) {
      renderGame(
        this,
        this.#playerId,
        this.#settings,
        this.#state,
        this.#handleAction
      );
    }
  }

  connectedCallback() {
    this.render();
  }
}

export class Reference extends HTMLElement {
  #settings?: GameSettings;

  set settings(settings: GameSettings) {
    this.#settings = settings;
    this.setAttribute("settings", JSON.stringify(settings));
  }

  connectedCallback() {
    this.render();
  }

  static get observedAttributes() {
    return ["settings"];
  }

  attributeChangedCallback() {
    this.render();
  }

  private render() {
    if (this.#settings) {
      renderReference(this, this.#settings);
    }
  }
}

export class Settings extends HTMLElement {
  constructor() {
    super();

    this.#onChange = (settings: GameSettings) => {
      this.dispatchEvent(
        new CustomEvent("settings_change", {
          detail: settings,
        })
      );
    };
  }

  #onChange: Function;
  #settings?: GameSettings;

  get #readonly(): boolean {
    const attr = this.attributes.getNamedItem("readonly");
    return attr !== null && attr.value !== "false";
  }

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.render();
    }
  }

  private render() {
    renderSettings(
      this,
      this.#settings ?? defaultSettings,
      this.#onChange,
      this.#readonly
    );
  }

  connectedCallback() {
    this.render();
  }

  static get observedAttributes() {
    return ["readonly"];
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (name === "readonly") {
      this.render();
    }
  }
}

export const defaultSettings: GameSettings = {
  "selected-player-counts": [3, 4, 5, 6],
};

export function playerCounts(settings: GameSettings): number[] {
  const validPlayerCounts = [2, 3, 4, 5, 6];
  return validPlayerCounts.filter((c) =>
    settings["selected-player-counts"].includes(c)
  );
}
