import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/liars-dice/",

  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "liars-dice",
      name: "Gamesite6_LiarsDice",
    },
  },
  plugins: [react()],
});
