(ns liars-dice.rng-test
  (:require [clojure.test :refer :all])
  (:require [liars-dice.rng :as rng]))

(deftest rng-test
  (testing "same seed results in same dice rolls")
  (let [rng-a (rng/mk-rng 42)
        dice-a (take 5 (repeatedly #(rng/roll-die rng-a)))
        rng-b (rng/mk-rng 42)
        dice-b (take 5 (repeatedly #(rng/roll-die rng-b)))]
    (is (= dice-a dice-b)))
  (testing "next rolls with same rng are different")
  (let [rng (rng/mk-rng 42)
        dice-a (take 5 (repeatedly #(rng/roll-die rng)))
        dice-b (take 5 (repeatedly #(rng/roll-die rng)))]
    (is (not= dice-a dice-b))))