(ns liars-dice.core-test
  (:require [clojure.test :refer :all]
            [liars-dice.core :refer :all]))

(deftest test-get-next-player-id
  (let [players [{:id 1 :dice [1 2 3]}
                 {:id 2 :dice []}
                 {:id 3 :dice [6 6 4]}
                 {:id 4 :dice [1 3 6 4 3]}]]
    (testing "next player is in next position"
      (is (= (get-next-player-id players 3) 4)))
    (testing "cycles back to first position"
      (is (= (get-next-player-id players 4) 1)))
    (testing "skips players without dice"
      (is (= (get-next-player-id players 1) 3)))
    (testing "if current player has no dice, skips to next normally"
      (is (= (get-next-player-id players 2) 3)))))

(deftest test-find-index
  (is (= 2 (find-index [false false true] identity)))
  (is (= nil (find-index [false false false] identity)))
  (is (= 1 (find-index [{:id 1} {:id 2}, {:id 3}] #(= (:id %) 2)))))
