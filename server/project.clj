(defproject liars-dice-server "0.2.1-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "AGPL-3.0-or-later"}
  :dependencies [[org.clojure/clojure "1.10.3"],
                 [ring/ring-core "1.9.4"],
                 [ring/ring-jetty-adapter "1.9.4"]
                 [metosin/reitit "0.5.15"]]
  :main ^:skip-aot liars-dice.server
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
