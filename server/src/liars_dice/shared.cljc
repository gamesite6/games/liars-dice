(ns liars-dice.shared)

(defn successful-bid?
  [{players :players {goal-count :count die-value :die} :phase}]
  (let [all-dice-values (flatten (map :dice players))
        match-count (count (filter #(= % die-value) all-dice-values))]
    (<= goal-count match-count)))

(defn has-dice [player]
  (seq (:dice player)))

(defmulti acting? (fn [state player-id] (:kind (:phase state))))
(defmethod acting? "leading" [state player-id] (= player-id (:player (:phase state))))
(defmethod acting? "following" [state player-id] (= player-id (:player (:phase state))))
(defmethod acting? "challenge-result" [state player-id]
  (and
    (some #(and (= (:id %) player-id) (has-dice %)) (:players state))
    (not (contains? (set (:ready (:phase state))) player-id))))

(defmethod acting? "final-leading" [state player-id] (= player-id (:player (:phase state))))
(defmethod acting? "final-following" [state player-id] (= player-id (:player (:phase state))))
(defmethod acting? "final-challenge-result" [state player-id]
  (and
    (some #(and (= (:id %) player-id) (has-dice %)) (:players state))
    (not (contains? (set (:ready (:phase state))) player-id))))

(defmethod acting? "game-complete" [_ _] false)

(defn contains-value? [coll value] (some #{value} coll))

(def valid-player-counts [2 3 4 5 6])

(defn validated-player-counts [selected-player-counts]
  (filter #(contains-value? valid-player-counts %) selected-player-counts))