(ns liars-dice.rng
  (:gen-class)
  (:import (java.util Random)))

(defn mk-rng [seed] (Random. seed))

(defn roll-die [rng]
  (+ 1 (.nextInt rng 6)))

(defn pick-random [rng xs]
  (nth xs (.nextInt rng (count xs))))