(ns liars-dice.core
  (:gen-class)
  (:require [liars-dice.rng :as rng]
            [clojure.spec.alpha :as s]
            [liars-dice.shared :as shared]))

(defrecord Settings [selected-player-counts])

(defrecord Player [id dice])
(defrecord State [players phase])

(defrecord LeadingPhase [kind player])
(defrecord FollowingPhase [kind leader count die player])
(defrecord ChallengeResultPhase [kind leader count die challenger ready])

(defrecord FinalLeadingPhase [kind player])
(defrecord FinalFollowingPhase [kind leader bid player])
(defrecord FinalChallengeResultPhase [kind leader bid challenger ready])

(defrecord GameCompletePhase [kind])

(defrecord BidAction [kind count die])
(defrecord FinalBidAction [kind bid])
(defrecord ChallengeAction [kind])
(defrecord ReadyAction [kind])

(s/def ::playerCounts (s/coll-of int?))
(s/def ::selected-player-counts (s/coll-of int?))
(s/def ::settings (s/keys :req-un [::selected-player-counts]))

(s/def ::seed int?)
(s/def ::player-id int?)
(s/def ::count pos-int?)
(s/def ::die (s/and int? #(<= 1 % 6)))
(s/def ::bid pos-int?)

(defmulti action-spec :kind)
(defmethod action-spec "bid" [_] (s/keys :req-un [::count ::die]))
(defmethod action-spec "final-bid" [_] (s/keys :req-un [::bid]))
(defmethod action-spec "challenge" [_] map?)
(defmethod action-spec "ready" [_] map?)

(s/def ::action (s/multi-spec action-spec :kind))

(s/def :phase/ready (s/coll-of ::player-id))
(s/def :phase/player ::player-id)
(s/def :phase/leader ::player-id)
(s/def :phase/challenger ::player-id)

(defmulti phase-spec :kind)
(defmethod phase-spec "leading" [_] (s/keys :req-un [:phase/player]))
(defmethod phase-spec "following" [_] (s/keys :req-un [:phase/leader ::count ::die :phase/player]))
(defmethod phase-spec "challenge-result" [_] (s/keys :req-un [:phase/leader ::count ::die :phase/challenger :phase/ready]))
(defmethod phase-spec "final-leading" [_] (s/keys :req-un [:phase/player]))
(defmethod phase-spec "final-following" [_] (s/keys :req-un [:phase/leader ::bid :phase/player]))
(defmethod phase-spec "final-challenge-result" [_] (s/keys :req-un [:phase/leader ::bid :phase/challenger :phase/ready]))
(defmethod phase-spec "game-complete" [_] map?)

(s/def ::phase (s/multi-spec phase-spec :kind))
(s/def :state.player/id ::player-id)
(s/def ::dice (s/coll-of ::die))
(s/def ::player (s/keys :req-un [:state.player/id ::dice]))
(s/def :state/players (s/coll-of ::player))
(s/def ::state (s/keys :req-un [:state/players ::phase]))

(defn get-next-player-id
  [players current-player-id]
  (let [remaining-players (->> players
                               (filter #(or (= (:id %) current-player-id)
                                            (not-empty (:dice %))))
                               (map :id))
        player-idx (.indexOf remaining-players current-player-id)]
    (nth (cycle remaining-players) (inc player-idx))))

(defn all-ready? [ready players]
  (let [ready (set ready)
        player-ids (map :id (filter shared/has-dice         ; only the players who still have dice need to click ready
                                    players))]
    (every? #(contains? ready %) player-ids)))

(defn find-index [coll pred?]
  (loop [idx 0 coll coll]
    (if (empty? coll)
      nil
      (if (pred? (first coll))
        idx
        (recur (inc idx) (rest coll))))))

(defn re-roll-dice [player rng]
  (update player :dice (fn [dice] (map (fn [_] (rng/roll-die rng)) dice))))

(defn re-roll-all-dice [players rng]
  (map #(re-roll-dice % rng) players))

(defn count-dice [player] (-> player :dice count))

(defmulti perform-action (fn [state user-id action seed] [(:kind (:phase state)) (:kind action)]))

(defmethod perform-action ["leading" "bid"]
  [{players :players :as state} user-id action _]
  {:pre [(shared/acting? state user-id)]}
  (assoc state :phase
         (map->FollowingPhase {:kind   "following"
                               :leader user-id
                               :count  (:count action)
                               :die    (:die action)
                               :player (get-next-player-id players user-id)})))

(defmethod perform-action ["final-leading" "final-bid"]
  [{players :players :as state} user-id action _]
  {:pre [(shared/acting? state user-id)]}
  (assoc state :phase
         (map->FinalFollowingPhase {:kind   "final-following"
                                    :leader user-id
                                    :bid    (:bid action)
                                    :player (get-next-player-id players user-id)})))

(defmethod perform-action ["following" "bid"]
  [{players :players :as state} user-id action _]
  {:pre [(shared/acting? state user-id)
         (let [prev-count (get-in state [:phase :count])
               next-count (:count action)
               prev-die (get-in state [:phase :die])
               next-die (:die action)]
           (and (<= prev-count next-count)
                (or (< prev-count next-count) (< prev-die next-die))))]}
  (assoc state :phase
         (map->FollowingPhase {:kind   "following"
                               :leader user-id
                               :count  (:count action)
                               :die    (:die action)
                               :player (get-next-player-id players user-id)})))


(defmethod perform-action ["final-following" "final-bid"]
  [{players :players :as state} user-id action _]
  {:pre [(shared/acting? state user-id)
         (let [bid (:bid action)
               prev-bid (get-in state [:phase :bid])]
           (< prev-bid bid 13))]}
  (assoc state :phase
         (map->FinalFollowingPhase {:kind   "final-following"
                                    :leader user-id
                                    :bid    (:bid action)
                                    :player (get-next-player-id players user-id)})))

(defmethod perform-action ["following" "challenge"]
  [state user-id _ _]
  {:pre [(shared/acting? state user-id)]}
  (let [phase (:phase state)]
    (assoc state :phase (map->ChallengeResultPhase {:kind       "challenge-result"
                                                    :leader     (:leader phase)
                                                    :count      (:count phase)
                                                    :die        (:die phase)
                                                    :challenger user-id
                                                    :ready      #{}}))))

(defmethod perform-action ["final-following" "challenge"]
  [state user-id _ _]
  {:pre [(shared/acting? state user-id)]}
  (let [phase (:phase state)]
    (assoc state :phase (map->FinalChallengeResultPhase {:kind       "final-challenge-result"
                                                         :leader     (:leader phase)
                                                         :bid        (:bid phase)
                                                         :challenger user-id
                                                         :ready      #{}}))))

(defmethod perform-action ["final-challenge-result" "ready"]
  [{{ready         :ready
     leader-id     :leader
     challenger-id :challenger
     bid           :bid} :phase players :players :as state} user-id _ _]
  {:pre [(shared/acting? state user-id)]}
  (let [ready (set ready)
        next-ready (conj ready user-id)
        all-ready (and (contains? next-ready leader-id)
                       (contains? next-ready challenger-id))
        dice-sum (->> state :players (map :dice) flatten (apply +))
        loser-id (if (> bid dice-sum) leader-id challenger-id)
        loser-idx (find-index players #(= loser-id (:id %)))
        next-players (update-in players [loser-idx :dice] #(drop 1 %))]
    (if all-ready
      (-> state
          (assoc :players next-players)
          (assoc :phase (map->GameCompletePhase {:kind "game-complete"})))
      (assoc-in state [:phase :ready] next-ready))))

(defmethod perform-action ["challenge-result" "ready"]
  [{players :players {ready      :ready
                      challenger :challenger
                      leader     :leader} :phase :as state} user-id _ seed]
  {:pre [(shared/acting? state user-id)]}
  (let [next-ready (conj ready user-id)]
    (if (all-ready? next-ready players)
      (let [success (shared/successful-bid? state)
            loser-id (if success challenger leader)
            loser-idx (find-index players #(= loser-id (:id %)))
            rng (rng/mk-rng seed)
            next-players (-> players
                             (update-in [loser-idx :dice] #(drop 1 %))
                             (re-roll-all-dice rng))
            game-is-complete (= 1 (count (filter shared/has-dice next-players)))
            total-dice (apply + (map count-dice next-players))]
        (-> state
            (assoc :players next-players)
            (assoc :phase (if game-is-complete
                            (map->GameCompletePhase {:kind "game-complete"})
                            (let [next-player-id (if (shared/has-dice (nth next-players loser-idx))
                                                   loser-id
                                                   (get-next-player-id next-players loser-id))]
                              (if (= 2 total-dice)
                                (map->FinalLeadingPhase {:kind   "final-leading"
                                                         :player next-player-id})
                                (map->LeadingPhase {:kind   "leading"
                                                    :player next-player-id})))))))
      (assoc-in state [:phase :ready] next-ready))))

(defn initial-player [rng player-id]
  (->Player player-id (take 5 (repeatedly #(rng/roll-die rng)))))

(defn initial-state
  [settings player-ids seed]
  (let [rng (rng/mk-rng seed)]
    (map->State
     {:players (map (partial initial-player rng) player-ids)
      :phase   (map->LeadingPhase {:kind   "leading"
                                   :player (rng/pick-random rng player-ids)})})))
