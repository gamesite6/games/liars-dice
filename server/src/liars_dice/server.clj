(ns liars-dice.server
  (:gen-class)
  (:require
    [ring.adapter.jetty :as jetty]
    [ring.middleware.params :as params]
    [liars-dice.core :as core]
    [liars-dice.shared :as shared]
    [reitit.ring.middleware.muuntaja :as muuntaja]
    [reitit.ring :as ring]
    [reitit.coercion.spec]
    [reitit.ring.coercion :as coercion]
    [clojure.spec.alpha :as s]
    [muuntaja.core :as m]))


(s/def :req/players (s/coll-of ::core/player-id))
(s/def :req/performedBy ::core/player-id)
(s/def :req/completed boolean?)
(s/def :req/nextState ::core/state)

(def app
  (ring/ring-handler
    (ring/router
      [["/info"
        {:post    {:parameters {:body (s/keys :req-un [::core/settings])}
                   :responses  {200 {:body (s/keys :req-un [::core/playerCounts])}}}
         :handler (fn [{{{{selected-player-counts :selected-player-counts} :settings} :body} :parameters}]
                    {:status 200 :body {:playerCounts (shared/validated-player-counts selected-player-counts)}})}]
       ["/initial-state"
        {:post    {:parameters {:body (s/keys :req-un [::core/seed ::core/settings :req/players])}
                   :responses  {200 {:body (s/keys :req-un [::core/state])}
                                422 {:body empty?}}}
         :handler (fn [{{{settings :settings
                          players  :players
                          seed     :seed} :body} :parameters}]
                    (if-let [state (core/initial-state settings players seed)]
                      {:status 200 :body {:state state}}
                      {:status 422}))}]
       ["/perform-action"
        {:post    {:parameters {:body (s/keys :req-un [:req/performedBy ::core/action ::core/state ::core/settings ::core/seed])}
                   :responses  {200 {:body (s/keys :req-un [:req/completed :req/nextState])}
                                422 {:body empty?}}}
         :handler (fn [{{{performed-by :performedBy
                          action       :action
                          state        :state
                          seed         :seed} :body} :parameters}]
                    (if-let [next-state (core/perform-action state performed-by action seed)]
                      {:status 200 :body {:nextState next-state :completed false}}
                      {:status 422}))}]]

      {:data {:muuntaja   m/instance
              :coercion   reitit.coercion.spec/coercion
              :middleware [params/wrap-params
                           muuntaja/format-middleware
                           coercion/coerce-exceptions-middleware
                           coercion/coerce-request-middleware
                           coercion/coerce-response-middleware]}})
    (ring/create-default-handler)))

(defn -main
  [& args]
  (let [port (Integer/parseInt (System/getenv "PORT"))]
    (jetty/run-jetty app {:port port})))